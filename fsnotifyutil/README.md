## 文件监控
### 使用demo

```
// 注册需要加载的文件，有变化动态加载
wm.Register("/data/qpconfig/message_map.json", c.MessageTable.Load)
```

```
// 配置文件监听
wm, err := fsnotifyutils.NewWatchManager()
if err != nil {
    elog.Errorf("fsnotifyutils: NewWatchManager: err:%v\n", err)
    return
}

wm.Register("/data/qpconfig/message_map.json", c.MessageTable.Load)

```