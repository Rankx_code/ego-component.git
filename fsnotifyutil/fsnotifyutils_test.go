package fsnotifyutil

import (
	"encoding/json"
	"fmt"
	"github.com/gotomicro/ego/core/elog"
	"io/ioutil"
	"os"
	"sync"
	"testing"
)

func TestFsNotify(t *testing.T) {
	// Context上下文

	var (
		ctx      = NewContext()
		filePath = "message_map.json"
	)

	wm, err := NewWatchManager()
	if err != nil {
		elog.Error("fsnotifyutil: NewWatchManager: err", elog.FieldErr(err))
		return
	}

	wm.Register(filePath, ctx.FileCache.Load)

	t.Log(ctx.FileCache)

	select {}
}

type Context struct {
	FileCache *FileCache // 消息ID与目标服务器ID对应
}

func NewContext() *Context {
	c := &Context{
		FileCache: NewFileCache(),
	}
	return c
}

type FileCache struct {
	sync.RWMutex
	table map[int32]int32 // 记录表
}

func NewFileCache() *FileCache {
	return &FileCache{table: make(map[int32]int32)}
}

// SetTable 设置整表
func (t *FileCache) SetTable(table map[int32]int32) error {
	t.Lock()
	defer t.Unlock()

	t.table = table
	return nil
}

// NotifyCallback 文件变化时重新加载表
func (t *FileCache) Load(fileName string) error {
	fmt.Printf("load file ,fileName=%v \n", fileName)

	f, err := os.Open(fileName)
	if err != nil {
		return fmt.Errorf("error: FileCache: Load: %v", err)
	}

	configBytes, err := ioutil.ReadAll(f)
	if err != nil {
		return fmt.Errorf("error: FileCache: Load: ioutil.ReadAll: %v", err)
	}

	table := make(map[int32]int32)
	if err = json.Unmarshal(configBytes, &table); err != nil {
		return fmt.Errorf("error: FileCache: Load: json.Unmarshal: %v\n", err)
	}

	// 使用最新的配置表
	t.SetTable(table)
	fmt.Printf("load file success ,fileName=%v,table=%v \n", fileName, table)
	return nil
}
