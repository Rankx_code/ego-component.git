module gitee.com/Rankx_code/ego-component

go 1.21.0

require (
	github.com/benbjohnson/clock v1.3.5 // indirect
	github.com/juju/ratelimit v1.0.2 // indirect
	github.com/tealeg/xlsx v1.0.5 // indirect
	go.uber.org/ratelimit v0.3.1 // indirect
)
