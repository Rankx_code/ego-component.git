package zredis

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"github.com/gotomicro/ego/core/elog"
	"testing"
	"time"
)

func TestNewLazyPool(t *testing.T) {
	p := NewLazyPool("49.234.137.226:6379",
		"",
		0,
		1,
		1)
	num := 5
	var conns []redis.Conn
	for i := 0; i < num; i++ {
		conns = append(conns, p.Get())
	}
	for i, conn := range conns {
		v, err := redis.String(conn.Do("get", "kv-k"))
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(i, v)
	}
	fmt.Println("success")
}

func TestPipeline(t *testing.T) {
	var loggerOptions []elog.Option
	loggerOptions = append(loggerOptions,
		elog.WithDebug(true))
	elog.DefaultLogger = elog.DefaultContainer().Build(loggerOptions...)
	p := NewLazyPool("49.234.137.226:6379",
		"",
		0,
		1,
		1)
	num := 5
	var conns []redis.Conn
	for i := 0; i < num; i++ {
		conns = append(conns, p.Get())
	}
	for i, c := range conns {
		c.Send("MULTI")
		c.Send("INCR", "foo")
		c.Send("INCR", "bar")
		r, err := c.Do("EXEC")
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(i, r)

		v, err := redis.String(c.Do("get", "foo"))
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(i, v)
	}
	fmt.Println("success")
	time.Sleep(5 * time.Second)
}

func TestNewPool(t *testing.T) {
	p := newPool("49.234.137.226:6379",
		"",
		0,
		1,
		1)
	num := 5
	var conns []redis.Conn
	for i := 0; i < num; i++ {
		conns = append(conns, p.Get())
	}
	for i, conn := range conns {
		v, err := redis.String(conn.Do("get", "kv-k"))
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(i, v)
	}
	fmt.Println("success")
}
