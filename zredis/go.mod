module gitee.com/Rankx_code/ego-component/zredis

go 1.20

require (
	github.com/garyburd/redigo v1.6.3
	github.com/gotomicro/ego v1.1.10
	go.uber.org/zap v1.24.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-logr/logr v1.2.3 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/gotomicro/logrotate v0.0.0-20211108034117-46d53eedc960 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/spf13/cast v1.4.1 // indirect
	github.com/stretchr/testify v1.8.3 // indirect
	go.opentelemetry.io/otel v1.7.0 // indirect
	go.opentelemetry.io/otel/trace v1.7.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	google.golang.org/grpc v1.46.0 // indirect
)
