# zredis
当前旧项目使用redigo组件时，由于代码生成脚本需要传入redis.conn，导致每次请求redis连接占用时间过长。

这里新提供一种redis连接池，来代理原连接池get、close方法，使其快速归还连接。