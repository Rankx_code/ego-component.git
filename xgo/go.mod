module gitee.com/Rankx_code/ego-component/xgo

go 1.20

require (
	github.com/codegangsta/inject v0.0.0-20150114235600-33e0aa1cb7c0
	github.com/gotomicro/ego v1.1.17
	go.uber.org/multierr v1.11.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-logr/logr v1.2.4 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/gotomicro/logrotate v0.0.0-20211108034117-46d53eedc960 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/spf13/cast v1.4.1 // indirect
	go.opentelemetry.io/otel v1.18.0 // indirect
	go.opentelemetry.io/otel/metric v1.18.0 // indirect
	go.opentelemetry.io/otel/trace v1.18.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/zap v1.21.0 // indirect
	google.golang.org/grpc v1.58.1 // indirect
)
