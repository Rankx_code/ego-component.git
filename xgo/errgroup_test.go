package xgo

import (
	"context"
	"fmt"
	"net/http"
	"testing"
)

type ABC struct {
	CBA int
}

func TestNormal(t *testing.T) {
	var g Group
	var urls = []string{
		"http://www.zonst.com/",
		"http://www.baidu.com/",
		"http://www.1688.com/",
	}
	for _, url := range urls {
		url := url
		g.Go(func(context.Context) error {
			resp, err := http.Get(url)
			if err == nil {
				resp.Body.Close()
			}
			return err
		})
	}
	// Wait for all HTTP fetches to complete.
	if err := g.Wait(); err == nil {
		fmt.Println("Successfully fetched all URLs.")
	}
}

func TestRecover(t *testing.T) {
	var (
		g   Group
		err error
	)
	g.Go(func(context.Context) (err error) {
		panic("11111")
	})
	if err = g.Wait(); err != nil {
		t.Logf("error:%+v", err)
	}
	t.Log("continue run here")
	t.Log()
}
