package utils

// GetString 从map中获取string
func GetString(m map[string]interface{}, key string) string {
	if m == nil {
		return ""
	}

	vi, exist := m[key]
	if !exist {
		return ""
	}

	v, convert := vi.(string)
	if !convert {
		return ""
	}
	return v
}

// GetInt 从map中获取 int
func GetInt(m map[string]interface{}, key string) int {
	if m == nil {
		return 0
	}

	vi, exist := m[key]
	if !exist {
		return 0
	}

	switch v := vi.(type) {
	case float64:
		return int(v)
	case float32:
		return int(v)
	case int:
		return v
	default:
		return 0
	}
}

// GetInt64 从map中获取 int64
func GetInt64(m map[string]interface{}, key string) int64 {
	if m == nil {
		return 0
	}

	vi, exist := m[key]
	if !exist {
		return 0
	}

	switch v := vi.(type) {
	case float64:
		return int64(v)
	case float32:
		return int64(v)
	case int64:
		return v
	case int:
		return int64(v)
	default:
		return 0
	}
}
