package utils

const (
	FALSE = 1 // 否
	TRUE  = 2 // 是
)

const (
	// UserInfo key
	USERINFOKEY = "userInfo"
	// 默认组织
	DEFAULTORG = 1
)
