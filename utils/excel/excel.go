package excel

type SheetContent struct {
	Sheet int
	Rows  []RowContent
}

type RowContent struct {
	Row   int
	Cells []CellContent
}

type CellContent struct {
	Title string
	Cell  int
	Blank interface{}
}

type ExcelTool interface {
	// Read 读excel文件
	Read(file string) ([]SheetContent, error)

	// Writer 写excel文件
	Writer([]SheetContent) ([]byte, error)
}
