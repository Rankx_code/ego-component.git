package excel

import (
	"fmt"
	"gitee.com/Rankx_code/ego-component/utils/errorx"
	"github.com/tealeg/xlsx"
)

type TealegXlsx struct{}

func (TealegXlsx) Read(file string) ([]SheetContent, error) {
	// 打开 Excel 文件
	excel, err := xlsx.OpenFile(file)
	if err != nil {
		fmt.Println(err)
		return nil, errorx.Wrap(err)
	}

	var csss []SheetContent
	for i, sheet := range excel.Sheets {
		fmt.Println("开始读取", file, fmt.Sprintf("[sheet_%d][%s]", i, sheet.Name))
		var css []RowContent
		for j, row := range sheet.Rows {
			var cs []CellContent
			for k, cell := range row.Cells {
				title := sheet.Rows[0].Cells[k].String()
				cs = append(cs, CellContent{Title: title, Cell: k, Blank: cell})
			}
			css = append(css, RowContent{Row: j, Cells: cs})
		}

		fmt.Println("读取完成", file, fmt.Sprintf("[sheet_%d][%s]", i, sheet.Name))
		csss = append(csss, SheetContent{Sheet: i, Rows: css})
	}

	return csss, nil
}

func (TealegXlsx) Writer(sc []SheetContent) ([]byte, error) {
	return nil, nil
}
