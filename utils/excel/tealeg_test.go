package excel

import (
	"github.com/gotomicro/ego/core/util/xstring"
	"testing"
)

func TestTealegXlsx_Read(t *testing.T) {
	var (
		tea TealegXlsx
	)
	if content, err := tea.Read("./test.xlsx"); err != nil {
		t.Fatal(err)
	} else {
		t.Log(xstring.PrettyJSON(content))
	}
}

func TestTealegXlsx_Writer(t *testing.T) {
	var (
		tea TealegXlsx
		sc  []SheetContent
	)

	if content, err := tea.Writer(sc); err != nil {
		t.Fatal(err)
	} else {
		t.Log(xstring.PrettyJSON(content))
	}
}
