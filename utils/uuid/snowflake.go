package uuid

import (
	"github.com/bwmarrin/snowflake"
	"github.com/gotomicro/ego/core/elog"
	"go.uber.org/zap"
	"math/rand"
	"sync"
)

var (
	SnowFlakeNode *snowflake.Node
	once          sync.Once
)

func GetSnowFlakeId() string {
	once.Do(initSnowFlakeNode)
	return SnowFlakeNode.Generate().String()
}

func initSnowFlakeNode() {
	var err error
	SnowFlakeNode, err = snowflake.NewNode(int64(rand.Intn(100)))
	if err != nil {
		elog.Panic("snowflake 初始化失败", zap.Error(err))
	}
}
