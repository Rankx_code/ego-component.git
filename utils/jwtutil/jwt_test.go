package jwtutil

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"testing"
	"time"
)

func TestGenerateJWT(t *testing.T) {
	JwtTool.SetSecretKey("HELLO")
	fmt.Println(JwtTool.GenerateJWT(map[string]interface{}{
		"user_id": int(1),
		"version": 1,
		"exp":     time.Now().Add(2 * time.Hour).Unix(),
	}))
}

func TestValidateJWT(t *testing.T) {
	JwtTool.SetSecretKey("88mkva4N{3m47/sm")
	token, msg := JwtTool.ValidateJWT("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0MTcsInVzZXJfbmFtZSI6IuW8oOWuh-mUiyIsImdyb3VwX2lkIjoyLCJpc19zdXBlcnVzZXIiOmZhbHNlLCJleHAiOjE2MTI3NzA1MTZ9.NphCnvVc_J48QOsALEiUa0vVYYI90Gc78M8vse6ueQQ")

	if !token.Valid {
		fmt.Println("valid fail", msg)
		return
	}

	var r map[string]interface{}
	r = token.Claims.(jwt.MapClaims)

	fmt.Println(r)
}
