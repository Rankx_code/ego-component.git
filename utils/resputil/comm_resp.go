package resputil

import (
	"encoding/json"
	"fmt"
)

// Successed 成功的默认响应
var Successed = CommResp{}

// CommResp 通用响应包
type CommResp struct {
	ErrNo      int32       `json:"errno"`
	ErrMsg     string      `json:"errmsg"`
	Data       interface{} `json:"data,omitempty"`
	ErrorData  interface{} `json:"errorData,omitempty"`
	StackTrace interface{} `json:"stackTrace,omitempty"`
}

func (r CommResp) String() string {
	body, err := json.Marshal(r)
	if err != nil {
		return fmt.Sprintf("CommResp: errno:%d,errmsg:%s, err:%v", r.ErrNo, r.ErrMsg, err)
	}
	return string(body)
}

// WithErrNo 设置errno
func (r CommResp) WithErrNo(errno int32) CommResp {
	r.ErrNo = errno
	return r
}

// WithErrMsg 设置errmsg
func (r CommResp) WithErrMsg(errmsg string) CommResp {
	r.ErrMsg = errmsg
	return r
}

// WithErrMsg 设置errmsg
func (r CommResp) WithErr(err error) CommResp {
	if err == nil {
		return r
	}
	errCode, msg := ParseError(err)
	r.ErrNo = errCode
	r.ErrMsg = msg
	r.StackTrace = err
	return r
}

// WithData 设置data
func (r CommResp) WithData(data interface{}) CommResp {
	r.Data = data
	return r
}

// WithErrorData 设置data
func (r CommResp) WithErrorData(errData interface{}) CommResp {
	r.ErrorData = errData
	return r
}

// WithStackTrace 设置 StackTrace
func (r CommResp) WithStackTrace(trace interface{}) CommResp {
	r.StackTrace = trace
	return r
}
