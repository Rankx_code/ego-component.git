package resputil

import (
	"fmt"
	"gitee.com/Rankx_code/ego-component/utils/errorx"
	"testing"
)

func TestParseError(t *testing.T) {
	err1 := errorx.Empty()
	code, msg := ParseError(err1)
	fmt.Println(code, msg)

	sErr := errorx.NewServiceError("test", 999)
	code, msg = ParseError(sErr)
	fmt.Println(code, msg)

	err2 := errorx.Wrap(sErr)
	code, msg = ParseError(err2)
	fmt.Println(code, msg)

	err3 := fmt.Errorf("test-xxx")
	code, msg = ParseError(err3)
	fmt.Println(code, msg)

	err4 := errorx.Wrap(err3)
	code, msg = ParseError(err4)
	fmt.Println(code, msg)
}

func TestFailed(t *testing.T) {
	err1 := errorx.Empty()
	resp := OnFailed(err1)
	fmt.Println(resp)

	sErr := errorx.NewServiceError("test", 999)
	resp = OnFailed(sErr)
	fmt.Println(resp)

	err2 := errorx.Wrap(sErr)
	resp = OnFailed(err2)
	fmt.Println(resp)

	err3 := fmt.Errorf("test-xxx")
	resp = OnFailed(err3)
	fmt.Println(resp)

	err4 := errorx.Wrap(err3)
	resp = OnFailed(err4)
	fmt.Println(resp)
}
