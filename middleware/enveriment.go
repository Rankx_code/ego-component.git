package middleware

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// OnlyDev 测试环境生效
func OnlyDev(mode string) gin.HandlerFunc {
	return func(c *gin.Context) {
		switch mode {
		case "local", "dev", "test":
			c.Next()
		default:
			c.JSON(http.StatusOK, gin.H{"errno": "-1", "errmsg": "生产不启动请求此接口"})
			c.Abort()
			return
		}
	}
}
