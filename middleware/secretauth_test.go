package middleware

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/Rankx_code/ego-component/utils/errorx"
	"github.com/gin-gonic/gin"
	"github.com/gotomicro/ego/core/util/xstring"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

func init() {
	router := gin.New()
	ping := router.Group("/v1/heartbeat")
	{
		ping.POST("/ping", func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, gin.H{"errno": 0, "errmsg": "ping success"})
		})
	}

	internal := router.Group("/v1/test")
	{
		authTable := map[string]string{
			AppId: AppSecret,
		}
		internal.Use(SecretAuth(authTable))
		{
			internal.POST("/get", func(ctx *gin.Context) {
				ctx.JSON(http.StatusOK, gin.H{"errno": 0, "errmsg": "get success"})
			})
		}
	}

	router.Run(":8080")
}

const (
	AppId     = "3c49995cda17777b5fb5"
	AppSecret = "c6d25a864c00a154d98f7a13d57a21fd"
)

func TestCalculcateSign(t *testing.T) {
	type Resp struct {
		Errno  int         `json:"errno"`
		Errmsg string      `json:"errmsg"`
		Data   interface{} `json:"data"`
	}

	var (
		userId = 1
		resp   = Resp{}
	)

	values := map[string]string{
		"user_id":   fmt.Sprintf("%d", userId),
		"timestamp": fmt.Sprintf("%v", time.Now().Unix()),
		"appid":     AppId,
	}
	sign := CalculcateSign(values, AppSecret)
	values["sign"] = sign

	url := "http://127.0.0.1:8080/v1/test/get"
	if err := HttpPostJSON(url, values, &resp); err != nil {
		t.Fatal(err.Error())
	}

	t.Log(xstring.PrettyJSON(resp))

}

var client = http.Client{
	Timeout: 5 * time.Second,
}

// 发送post请求
func HttpPostJSON(url string, payload interface{}, dest interface{}) error {
	buf, e := json.Marshal(payload)
	if e != nil {
		return errorx.Wrap(e)
	}

	req, e := http.NewRequest("POST", url, bytes.NewReader(buf))
	if e != nil {
		return errorx.Wrap(e)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, e := client.Do(req)
	if e != nil {
		return errorx.Wrap(e)
	}

	if resp == nil {
		return errorx.NewFromString("resp nil")
	}
	if resp.Body == nil {
		return errorx.NewFromString("resp.Body nil")
	}

	defer resp.Body.Close()
	resBuf, e := ioutil.ReadAll(resp.Body)
	if e != nil {
		return errorx.Wrap(e)
	}

	if e := json.Unmarshal(resBuf, dest); e != nil {
		return errorx.Wrap(e)
	}

	if resp.StatusCode != 200 {
		return errorx.NewFromString(StringMaxLen(200, resBuf))
	}

	return nil
}

func StringMaxLen(max int, buf []byte) string {
	if len(buf) == 0 {
		return ""
	}
	if max >= len(buf) {
		return string(buf)
	}
	return string(buf[:max])
}
